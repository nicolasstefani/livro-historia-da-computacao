# Surgimento das Calculadoras Mecânicas

## Primeiras calculadoras
### Ábaco
Inventado pelos chineses cerca de 5 séculos a.C, o ábaco é considerada a primeira calculadora da história. Ele era composto de fios ou varetas paralelas e sementes secas que deslizavam pelos fios.
Capaz de realizar cálculos de soma e subtração, ainda é utilizada em diversas regiões asiáticas e em escolas para o desenvolvimento do raciocinio lógico. 
Ele foi um instrumento que revolucionou a matemática, e se tornou o principal mecanismo de cálculo até a criação da Pascalina.

![](https://www.revistamacau.com/wp-content/uploads/2012/11/dreamstime_xxl_10753867.jpg)

### La Pascaline
Aproximadamente XXIII séculos depois, surgiu La Pascaline ou Pascalina. Foi a primeira calculadora mecânica, criada entre 1642-1644 por Blaise Pascal que tinha como objetivo agilizar o trabalho do seu pai, é a calculadora decimal conhecida mais antiga.
A Pascalina era uma grande caixa e contava com algumas engrenagens com 10 dentes, onde cada dente correspondia a um algarismo de 0 a 9. Ela era capaz apenas de somar e subtrair, multiplicaçoes e divisoes poderiam ser feitas atravez de somas e subtraçoes sucessivas, porem era um método demorado.
Apesar de ser algo inovador naquela época a Pascalina não fez muito sucesso comercialmente, pois era um instrumento muito caro e na época o mais próximo a uma calculadora era o Abáco.

![](http://1.bp.blogspot.com/-KGfdZ4ztRGA/VVXTwiK24cI/AAAAAAAACbE/JFXKC4IY9Ik/s1600/10-lec1-doc1-pascaline_copy.jpg)

### Stepped Reckoner - Roda Graduada 
Primeira calculadora capaz de realizar as quatro operações, inventada pelo alemão Gottfried Leibniz, possuia uma construção mecânica muito avançada.
A Stepped Reckoner fazia as quatro operações e a raiz quadrada, mas como Leibniz não chegou a termina-la, ela apresentava erros ao fazer cálculos de divisão e raiz. Devido seu custo ainda ser bastante elevado, apenas algumas pessoas podiam utilizar esse instrumento.

![](https://4.bp.blogspot.com/-3TPGPNu6hvo/Uogg9vfEYjI/AAAAAAAABz4/Uzl4Sx8Osx0/s400/stepped-reckoner-de-leibniz.jpg)

### Aritmómetro
Patenteado por Thomas de Colmar em 1820, o aritmometro foi a primeira calculadora mecanica bem sucedida comercialmente. Produzida de 1851 á 1915, ela era capaz de somar, subtrair, realizar multiplicações longas e divisões. Por 40 anos foi a única calculadora mecânica vendida no mundo, ao final desse periodo, algumas empresas começaram a vender clones do Aritmómetro.

![](https://2.bp.blogspot.com/-U1RKmBBpSHE/UogiIdK6txI/AAAAAAAAB0Q/-yof9bMNDqA/s320/arithmometre-thomas-colmar.png)

## Atualmente
Atualmente as calculadoras conseguem além de fazer as operações básicas, podem calculas funções trigonométricas normais e inversas(alguns modelos conseguem até reproduzir graficos das funções), também podem armazenar dados e instruções na memória, parecido a computadores. Muitas calculadoras utilizam módulos pré-programados e reversíveis de software.

![](https://m.media-amazon.com/images/I/71YJh0vg5aL._AC_SX355_.jpg)

# Referências:

1.  FONSECA FILHO, Cléuzio. História da computação: O Caminho do Pensamento e da Tecnologia. EDIPUCRS, 2007.

2. KASPERSKY, EUGENE. A história incalculada das calculadoras mecânicas. In: Eugene Kaspersky.  5 ago. 2016. Disponível em: https://eugene.kaspersky.com.br/2016/10/05/a-historia-incalculada-das-calculadoras-mecanicas/. Acesso em: 13 ago. 2021. 

3. MARCOLINI, Neldson. Máquina de calcular. 2 maio 2002. Disponível em: https://revistapesquisa.fapesp.br/maquina-de-calcular/. Acesso em: 13 ago. 2021.

4. A EVOLUÇÃO dos cálculos e das calculadoras. 27 maio 2015. Disponível em: https://blog.certisign.com.br/a-evolucao-dos-calculos-e-das-calculadoras/. Acesso em: 13 ago. 2021.


[Anterior](../README.md)
