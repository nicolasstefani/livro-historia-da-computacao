# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice

1. [Surgimento das Calculadoras Mecânicas](capitulos/surgimento_das_calculadoras_mecanicas.md)
1. [Primeiros Computadores](capitulos/primeiros_computadores.md)
1. [Evolução dos Computadores Pessoais e sua Interconexão](capitulos/Evolucao_dos_Computadores_Pessoais_e_sua_Interconexao.md)
1. [Computação Móvel](capitulos/Computacao_Movel.md)





## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168707/avatar.png?width=400)  | Nicolas Costa Stefani | nicolasstefani | [nicolasstefani@alunos.utfpr.edu.br](mailto:nicolasstefani@alunos.utfpr.edu.br)
