# Evolução dos PCs
--------------------
os PCs ou computadores de mesa tem sua origem para o público em geral em meados dos anos 1970, por conta dos preços altos na época poucas pessoas tinha condições de possuir um computador pessoal e com isso o mercado de PCs ainda estava engatinhando para se tornar o que é hoje uma industria gigante e muito importante.

## Década de 1970: surgem os PCs
----------------------------------------
Ainda há muita controvérsia de qual seria o primeiro computador pessoal criado, mas muitos sugerem que o Kenbak-1 lançado em 1971 pela empresa kenbak foi o primeiro a ser criado, o dispositivo continha 256 bytes de memória e usava interruptores de luz para operar. 

![](https://gildev.dev/content/2-projets/9-repliquekenbak-1/Kenbak-1.jpg)

5 Anos após o lançamento do Kenbak-1 surgiu no mercado o Apple-I da empresa Apple que foi o predecessor de um dos PCs mais famosos já comercializados o Apple-II lançado em 1977, o computador contava com 4 Kb de memória, um display de 280x192 pixels com quatro cores, o grande diferencial do Apple-II foi a possibilidade de adicionar periféricos como teclado e até de trocar peças.

![](https://ids.si.edu/ids/deliveryService?id=NMAH-DOR2011-8763&max=1000)

## década de 1980: surgimento dos primeiros laptops e IBM PC
------------------------
Em 1981 encorajada pelo sucesso que o Apple-II tinha se tornado a IBM lançou seu primeiro computador de mesa o IBM PC com memória de 16 Kb expansível para até 640 Kb e um processador Intel 8088, o PC da IBM tinha diferencial de ser mais intuitivo do que o computador da sua rival Apple assim democratizando o acesso a computadores.

![](https://s2.glbimg.com/TWAhsxEShUSzlhGlR49qAnbmLtc=/512x320/smart/e.glbimg.com/og/ed/f/original/2016/08/15/ibm-pc.jpg)

Outro mercado que foi iniciado na década de 1980 foi o mercado de laptops com o Grid Compass lançado em 1981 pela GRiD Systems Corp., o "portatil" de 5 kg continha uma tela de 320x240 pixels, embóra seu projeto fosse interessante o alto preço acabou não chamando a atenção do público em geral.

![](https://farm4.static.flickr.com/3483/3276224417_e49dbf2936_o.jpg)

## década de 1990: surgimento da internet e do windows 95
-------------------------
Na década de 1990 começaram a surgir diversos softwares como sistema operacional windows 95 e diversos hardwares como placas de vídeo com aceleração em 3D como a S3 ViRGE lançada em 1995, e a criação da internet como é hoje.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/f/ff/S3virgedx_97.jpg/500px-S3virgedx_97.jpg)

Todas estas inovações fizeram com que o computador de mesa mudasse seu estilo, sendo composto por gabinete, monitor, teclado e mouse.

![](https://cdn.mundodastribos.com/2012/04/437429-a-evolucao-dos-computadores-fotos-12.jpg)

## Década de 2000: novas tecnologias e evolução exponencial do hardware
-----------
Com o ínicio dos anos 2000 tanto o hardware quanto o software dos computadores sofreram uma enorme evolução como por exemplo a criação de telas LCDs para computadores e laptops e a modernização de processadores e placas de vídeo.

![](http://www.blogolandialtda.com.br/img-upload/images/Computadores.jpg)

## Década de 2010: conectividade e maior democratização da informática
--------------------------

Na década de 2010 com o constante avanço tecnológico foi popularizada a conectividade entre aparelhos como celulares com computadores e até com eletrodomésticos por meio da internet das coisas fazendo assim com que computadores de mesa possam ser controlados por celulares e afins, outra novidade foi a inteligencia artificial que possibilitou a automatização de diversos processos que antes eram demorados.

![](https://www.umov.me/wp-content/uploads/2020/05/Voc%C3%AA-sabe-o-que-%C3%A9-Internet-das-Coisas-IoT.jpg)

A década de 2010 não foi diferente da década de 2000, acontecendo assim diversas evoluções de hardware como processadores com cada vez mais núcleos e placas de vídeo cada vez mais avançadas, e periféricos cada vez mais responsivos e avançados como teclados e mouses.

![](https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQSBXLhld2Q3YfphkmEspgeHj7BuTdhS-7BHA&usqp=CAU)

# Refêrencias
----------------------

1. Gadelha, Julia, A evolução dos computadores, disponivel em: <http://www.ic.uff.br/~aconci/evolucao.html>
2. Garrett, Filipe, Dia da Informática: veja a evolução dos PCs ao longo das décadas, Techtudo, 2019.
3. Antunes, Ricardo, A evolução das placas de vídeo, Tecmundo, 2009.


[Anterior](../README.md)
