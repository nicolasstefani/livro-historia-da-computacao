# Primeiros Computadores

## O Início da Programação

O princípio da ideia da programação foi muito influenciado pelo Tear.
Em 1801, foi desenvolvido por Joseph Marie Jacquard, após analisar os problemas que a sua indústria de tecidos passava visto que todo seu trabalho era manual, desenvolvou a primeira máquina programável, buscando poder recortar automaticamente os tecidos.
O "Tear Programável", como foi chamado mais tarde, era programado através de cartões perfuráveis, da forma que Jaquard realizava as perfurações de acordo com o formato desejado no tecido e assim a máquina era capaz de reproduzir a forma no tecido.

## Charles Babbage

![](https://gitlab.com/trabalho-ie-livro/livro-historia-da-computacao/-/raw/master/imagens/Babbage.jpg)

Nascido em Londres(26 de dezembro de 1791 - 18 de outubro de 1871), Babbage foi um importante cientista, matemático, filósofo, egenheiro mecânico e inventor inglês que originou o conceito de computador programável.

### A Máquina de Diferenças

Em 1822, Charles Babbage havia publicado um artigo científico contendo informações sobre a chamada "Máquina de Diferenças": a qual Charles afirmou ser capaz de calcular funções de diversas naturezas, como trigonometria e logaritmos, de forma simples.

![](https://gitlab.com/trabalho-ie-livro/livro-historia-da-computacao/-/raw/master/imagens/MaqDif.jpeg)

Apesar de causar um alvoroço por conta de seu projeto estar muito a frente do tempo, a máquina só foi possível de ser implementada após anos devido a limitações de caráter técnico e financeiro.

### Engenho Analítico

Em 1837, Charles Babbage o Engenho Analítico, uma nova máquina que utilizava conceitos parecidos com os do Tear Programável, eram fornecidas intruções e comandos para a máquina através de cartões e a sua precisão chegava a até 50 casas decimais.
Infelizmente, mais uma vez pelo mesmo motivo da Máquina de Diferenças, o Engenho Analítico não foi capaz de ser implementado, inclusive também pelo motivo de não existir tecnologia suficientemente avançada para que o projeto fosse executado. 
Apesar de tudo isso, muitas das ideias utilizadas por Babbage são utilizadas até hoje.







O Primeiro Computador mecânico foi desenvolvido em 1890, pelo norte-americamno Hermann Hollerith.




# Referências:

1. A EVOLUÇÃO DOS COMPUTADORES. Disponível em: http://www.ic.uff.br/~aconci/evolucao.html#:~:text=Em%201946%2C%20surge%20o%20Eniac,realiza%204.500%20c%C3%A1lculos%20por%20segundo.

2. GUGIK, Gabriel. A história dos computadores e da computação. TecMundo, Curitiba, 2009.

‌



[Anterior](../README.md)
